#!/bin/bash

# requires 4 argument inputs:   
# 1: UNIQUE_ID - any unique string identifier  
# 2: CONDOR_PROCESS - condor process number  
# RUN_DIR - running directory (CMSSW_X_Y_Z/subdir)   
# mode.  should be 0 for background or 1 for signal

#
# header 
#

UNIQUE_ID=${1}
CONDOR_PROCESS=${2}
RUN_DIR=${3}

OUTDIR=${4}
INCUT_SET=${5}
PARMS_FILE=${6}
PSFILE=${7}

#HT_CUT=${8}
#PT1_CUT=${9}
#PT2_CUT=${10}
#PT3_CUT=${11}
#PT4_CUT=${12}
#ALPHAMAX_CUT=${13}
#MED_IP=${14}
#NEM_CUT=${15}
#NTRK1_CUT=${16}
#PVZ_CUT=${17}
#
#MET_CUT=${18}
#MASS=${19}
#MASS_CUT=${20}
#THETA2D_CUT=${21}
#IPSIG_CUT=${22}
#A2DSIG_CUT=${23}
#F2DSIG_CUT=${24}
#AHATE_CUT=${25}

echo ""
echo "CMSSW on Condor"
echo ""

START_TIME=`/bin/date`
echo "started at $START_TIME"

echo "check: Outdir = $OUTDIR"

#
# setup CMSSW software environment at UMD
#
export VO_CMS_SW_DIR=/sharesoft/cmssw
. $VO_CMS_SW_DIR/cmsset_default.sh
cd $RUN_DIR
eval `scramv1 runtime -sh`

FINAL_PREFIX_NAME=`echo ${UNIQUE_ID}_${CONDOR_PROCESS}`
FINAL_LOG=`echo $FINAL_PREFIX_NAME.log`

echo "output directory will be $OUTDIR"
#
# run c
#
cd -
#echo "1./main $CONDOR_PROCESS $OUTDIR $ICUT_SET $INCUT_SET $PARMS_FILE $HT_CUT $PT1_CUT $PT2_CUT $PT3_CUT $PT4_CUT $ALPHAMAX_CUT $MED_IP $NEM_CUT $NTRK1_CUT $PVZ_CUT $MET_CUT $MASS $MASS_CUT $THETA2D_CUT $IPSIG_CUT $A2DSIG_CUT $F2DSIG_CUT $AHATE_CUT"
#echo "2./main $CONDOR_PROCESS $OUTDIR $ICUT_SET $INCUT_SET $PARMS_FILE $8 $9 ${10} ${11} ${12} ${13} ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21} ${22} ${23} ${24} ${25}" 
#./main $CONDOR_PROCESS $OUTDIR $ICUT_SET $INCUT_SET $PARMS_FILE $8 $9 ${10} ${11} ${12} ${13} ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21} ${22} ${23} ${24} ${25}
echo "./main $CONDOR_PROCESS $OUTDIR $INCUT_SET $PARMS_FILE $PSFILE"
./main $CONDOR_PROCESS $OUTDIR $INCUT_SET $PARMS_FILE $PSFILE
#./main $CONDOR_PROCESS $OUTDIR $ICUT_SET $INCUT_SET $PARMS_FILE $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20 $21 $22 $23 $24
#>> $FINAL_LOG 2>&1

exitcode=$?

#
# end run
#

echo ""
END_TIME=`/bin/date`
echo "finished at $END_TIME"
exit $exitcode
