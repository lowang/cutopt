#!/bin/bash
pro=gN1
set -e
./scan_${pro}.py -s ModelA 
echo "submitting batch jobs"
./massjobs.sh
./scan_${pro}.py -s ModelB ; ./massjobs.sh
./scan_${pro}.py -s ModelD1 ; ./massjobs.sh
./scan_${pro}.py -s ModelD2 ; ./massjobs.sh
./scan_${pro}.py -s ModelD3 ; ./massjobs.sh
./scan_${pro}.py -s ModelD4 ; ./massjobs.sh
./scan_${pro}.py -s ModelD5 ; ./massjobs.sh
./scan_${pro}.py -s ModelD1mm ; ./massjobs.sh
./scan_${pro}.py -s ModelD150mm ; ./massjobs.sh
./scan_${pro}.py -s ModelG1 ; ./massjobs.sh
./scan_${pro}.py -s ModelG2 ; ./massjobs.sh
./scan_${pro}.py -s ModelG3 ; ./massjobs.sh
./scan_${pro}.py -s ModelG4 ; ./massjobs.sh
./scan_${pro}.py -s ModelG5 ; ./massjobs.sh
./scan_${pro}.py -s ModelG6 ; ./massjobs.sh
