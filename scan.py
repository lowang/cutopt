#!/usr/bin/env python

# for some reason now you have to run it twice.  
# it seems the .txt1 files are not quite there when the cat commands comes along

import subprocess
import os
import shutil
import sys
import optparse
import numpy as np
from itertools import product
#from optparse import OptionParser, OptionGroup

parser = optparse.OptionParser(usage="usage: python scan.py -s [QCD, ModelA, ModelB] ",
  description="This is a script to set up and run a scan over the kinematic cuts in the emerging jets analysis.")
parser.add_option ('-s','--sample', type='string', dest="sample", help="Sample to process (e.g., QCD)")

options, args = parser.parse_args()
if len(args) < 0:
  print "Too few arguments"
  parser.print_usage()
  exit(1)

#hostarea = "/data/users/eno/emergingJets5/CMSSW_7_6_3/src/EmergingJetAnalysis/histsQCD/"
#exearea = "/data/users/eno/emergingJets5/CMSSW_7_6_3/src/EmergingJetAnalysis/"
mainarea = os.getcwd()
hostarea = os.getcwd() + "/"


#Set the output directory name and parameter file based on the sample you are running over
#   ht pt1 pt2 pt3 pt4 #alpha medip ntrk1 pv_z_cut Nemerg met 
#   mass masscut theta2d/jmet ipsig a2dsig f2dsig
#OLDcutmin = [800,60,60, 50,50,    0.30,0.06 ,1,50,2,100,    600.,30000.,  0.,  2,  2,2,  10,0]
#OLDstep = [100,100,100,100,50,    0.04,0.05,0, 5,0,25.,    200.,  150.,100.,0.5,0.2,0.05,5,100]
##BESTCUT[1350,300,300,250,150.,    0.2,0.05,1,50.,2,0.,    850.,200.,0.,2,2,2,15] 
##BEST D5[100,60,60, 50,50,    0.25,0.1, 1,50,2,0.,     600.,??.,0.,2,2,2,10] 
#names = ["ht", "pt1", "pt2", "pt3", "pt4", "a3d", "medip", "ntrk1", "pv_z_cut", "Nemerg", "met", "mass", "masscut", "theta2d", "ipsig", "a2dsig", "f2dsig","ahate","jmet"]
# nef cef eta  ht pt1-4
# met ntrk1 pvz mass massc Nemg
# mip a3d ahat a3dz jmet theta2d
dirsuffix ="_D2emg3_pvz"
names = ["nef", "cef", "eta", "ht", "pt1", "pt2", "pt3", "pt4","met","ntrk1", "pvz_cut","mass", "masscut","Nemerg" , "medIP","a3d", "ahate","a3dz","jmet","theta2d"]
#cutmin = [0.9,0.9,2,1000, 300, 250, 200,100,   0,1,1.5,400,1e5,2,   0.05,0.25,8,0.01,1e5,0] #normal cut
cutmin = [0.9,0.9,2,1100, 300, 300, 200,150,    0,0,1.5,400,1e5,2,   0.03,0.25,6,0.008,1e5,0] #D3,4
step =   [0.,0.,0. ,100, 100, 100, 50,50,       50,1,.5,0,300,1,     0.02,0.05,2,0.002,75,0.1] 
nstep =  [1,1,1,      1,1,1,1,1, 		1,1,1,1,1,1,	      4,1,3,3,1,1]
dirname=""
parmsfile=""
parmsdir="configs/"
qnum = 1
#if options.sample=="QCD":
#  dirname="kinscan_QCD"+dirsuffix
#  parmsfile = "parms_QQCD.txt"
#  qnum = 8
if options.sample=="QCD2":
  dirname="kinscan_QCD2"+dirsuffix
  parmsfile = "parms_QCD2.txt"
  qnum = 36
elif options.sample=="ModelA":
  dirname="kinscan_modA"+dirsuffix
  parmsfile = "parms_modelA.txt"
elif options.sample=="ModelB":
  dirname="kinscan_modB"+dirsuffix
  parmsfile = "parms_modelB.txt"
elif options.sample=="ModelD1":
  dirname="kinscan_modD1"+dirsuffix
  parmsfile = "parms_modelD1.txt"
elif options.sample=="ModelD2":
  dirname="kinscan_modD2"+dirsuffix
  parmsfile = "parms_modelD2.txt"
elif options.sample=="ModelD3":
  dirname="kinscan_modD3"+dirsuffix
  parmsfile = "parms_modelD3.txt"
elif options.sample=="ModelD4":
  dirname="kinscan_modD4"+dirsuffix
  parmsfile = "parms_modelD4.txt"
elif options.sample=="ModelD5":
  dirname="kinscan_modD5"+dirsuffix
  parmsfile = "parms_modelD5.txt"
elif options.sample=="ModelD1mm":
  dirname="kinscan_modD1mm"+dirsuffix
  parmsfile = "parms_modelD1mm.txt"
elif options.sample=="ModelD150mm":
  dirname="kinscan_modD150mm"+dirsuffix
  parmsfile = "parms_modelD150mm.txt"
elif options.sample=="ModelF1":
  dirname="kinscan_modF1"+dirsuffix
  parmsfile = "parms_modelF1.txt"
elif options.sample=="ModelF2":
  dirname="kinscan_modF2"+dirsuffix
  parmsfile = "parms_modelF2.txt"
elif options.sample=="ModelF3":
  dirname="kinscan_modF3"+dirsuffix
  parmsfile = "parms_modelF3.txt"
elif options.sample=="ModelF4":
  dirname="kinscan_modF4"+dirsuffix
  parmsfile = "parms_modelF4.txt"
elif options.sample=="ModelF5":
  dirname="kinscan_modF5"+dirsuffix
  parmsfile = "parms_modelF5.txt"
elif options.sample=="ModelF6":
  dirname="kinscan_modF6"+dirsuffix
  parmsfile = "parms_modelF6.txt"
elif options.sample=="ModelF7":
  dirname="kinscan_modF7"+dirsuffix
  parmsfile = "parms_modelF7.txt"
else:
  print("Input sample not recognized!")
  parser.print_usage()
  exit(1)

#Create output directory; ask what to do if it exists already
print('Checking if directory {} exists...'.format(dirname))
if os.path.exists(hostarea+dirname):
  userreply = raw_input("Output directory already exists! Are you sure you want to overwrite all the contents? (y/n): ")
  if (userreply == "y"):
    print("Answer = yes. Old output directory will be deleted and a new empty one will be created.")
    shutil.rmtree(hostarea+dirname)
    os.makedirs(hostarea+dirname)
    os.makedirs(hostarea+dirname+"/stderr")
    os.makedirs(hostarea+dirname+"/stdout")
    os.makedirs(hostarea+dirname+"/log")
  elif (userreply == "n"):
    userreply = raw_input("Overwrite condor jobs configs? (y/n): ")
    if (userreply == "y"):
      print("Answer = yes. Overwrites condor jobs.")
    else:
      print("Answer = no. Exiting scan.py.")
      exit(1) 
  else:
    print("Invalid input. Exiting scan.py anyway.")
    exit(1)
else:
  os.makedirs(hostarea+dirname)
  os.makedirs(hostarea+dirname+"/stderr")
  os.makedirs(hostarea+dirname+"/stdout")
  os.makedirs(hostarea+dirname+"/log")
targetarea=hostarea+dirname+"/"
shutil.copy(hostarea+"scan_d.py",targetarea)

#Configure the scan

# Kak notes
# alpha/ meanip/ ntrk1: keep the same cut everywhere (fake rate)
# theta2d (may help in Longlive)
# met long lifetime
# ht pt mass (mediatior leading order)
print tuple(cutmin)
print 'cutmin: %.1f %.1f %.0f %d %d %d %d %d  %d %d %.0f %d %d %d    %.3f %.3f %.0f %.3f %d %.2f' %tuple(cutmin)
print 'steps: %.1f %.1f %.0f %d %d %d %d %d  %d %d %.0f %d %d %d    %.3f %.3f %.0f %.3f %d %.2f' %tuple(step)

#print 'cutmin: %.0f %.0f %.0f %.0f %.0f %.2f %.2f %d %.2f %d %.0f %.0f %.0f %.5f %.2f %.2f %.2f %.2f' %tuple(cutmin)
#print 'steps : %.0f %.0f %.0f %.0f %.0f %.2f %.2f %d %.2f %d %.0f %.0f %.0f %.5f %.2f %.2f %.2f %.2f' %tuple(step)

#write the .jdl file and the bash script to submit the condor job
f = open("massjobs.sh",'w')

# 3:ht 4-7:pt1-4 14:mip 15-17:a3d 18:jmet
plist  = [np.linspace(cutmin[i],cutmin[i]+step[i]*(nstep[i]-1),nstep[i]) for i in range(len(cutmin))]
#plist[14]=[0.01,0.1]
#plist[16]=[4,8,15,25,35]
#plist[16]= [0.5, 1, 2, 4, 6, 8, 10, 12, 15, 20, 25, 30]
for i in range(len(nstep)):
	print names[i], (plist[i][0] if len(plist[i])==1 else plist[i]) 

#check = lambda x: x[0]>=x[1]and x[1]>=x[2]and x[2]>=x[3]and x[3]>=x[4]
#fplist = [x for x in product(*plist) if check(x)]
fplist = [x for x in product(*plist)]
#these are checks for D3-5 with lower pt
check  = lambda x: x[4]>=x[5] and x[5]>=x[6] and x[6]>=x[7] and x[3]>sum(x[4:8])+50 and x[5]>x[7] and x[4]>x[6] and x[4]-x[7]>=100 and x[7]+x[8]<=x[3]/4
check2 = lambda x: (x[3]*.75-2*x[4]-2*x[6]-x[8]<=150 or x[3]==800) and x[4]-x[5]<150 and x[5]-x[6]<150 and x[6]-x[7]<100 and x[4]-x[7]<200
fplist = [x for x in product(*plist) if check(x) and check2(x)]
#fplist+=[(800.0, 200.0, 200.0, 100.0, 100.0, 100.0]
#fplist+=[(0.9, 0.9, 2, 1000., 200., 200., 100., 100., 100., 1, 1.5, 400, 100000, 2, 0.1, 0.25, 8, 0.01, 100000, 0.0)]
#for p in fplist:
#	print p[3:9]
#	print p
print fplist[0]
name = "kinescan-"+parmsfile
psfilename = targetarea+"ps-"+name
psfile = open(psfilename,"w")
for i, c in enumerate(fplist):
  #oldargstr = '%.0f %.0f %.0f %.0f %.0f %.2f %.2f %d %.2f %d %.0f %.0f %.0f %.5f %.2f %.2f %.2f %.2f' %c
  argstr = "%.1f %.1f %.0f %d %d %d %d %d  %d %d %.0f %d %d %d    %.3f %.3f %.0f %.3f %d %.2f\n" %c
  psfile.write(argstr)
  print '%2d '%i, argstr,
  #HTcut,pT1cut,pT2cut,pT3cut,pT4cut,alphacut,maxIPcut,nemcut,ntrk1cut,metcut,mass,masscut,theta2dcut,ipsigcut,a2dsigcut, f2dsigcut = 
psfile.close()


jdlfile = open(targetarea+"condor-jobs-"+name+".jdl","w")
jdlfile.write("universe = vanilla"+'\n')
jdlfile.write("Executable = "+mainarea+"/condor-executable.sh"+'\n')
jdlfile.write("should_transfer_files = NO"+'\n')
jdlfile.write("Requirements = TARGET.FileSystemDomain == \"privnet\""+'\n')
jdlfile.write("Output = "+targetarea+"stdout/"+name+"_sce_$(cluster)_$(process).stdout"+'\n')
jdlfile.write("Error = "+targetarea+"stderr/"+name+"_sce_$(cluster)_$(process).stderr"+'\n')
jdlfile.write("Log = "+targetarea+"log/"+name+"_sce_$(cluster)_$(process).condor"+'\n')
#argstr2 = "Arguments = %s $(process) %s %s %d %d %s %s \n" %(name, targetarea, targetarea, i , np.prod(nstep),parmsfile, argstr)
argstr2 = "Arguments = %s $(process) %s %s %d %s %s \n" %(name, targetarea, targetarea, len(fplist),
		parmsdir+parmsfile, psfilename)
print argstr2
jdlfile.write(argstr2)
#jdlfile.write("+IsHighPriorityJob = True \n")
jdlfile.write("Queue %i \n" %qnum)
jdlfile.close()

f.write("condor_submit "+dirname+"/condor-jobs-"+name+'.jdl'+'\n')

f.close()



