
#!/bin/bash

#set -e
#while IFS='' read -r line || [[ -n "$line" ]]; do
#    echo "Text read from file: $line"
#done < models.txt

#if [[ $# -eq 0 ]] ; then
#    echo 'usage ./modelrun.sh '
#    exit 1
#fi
for mx in 400 600 800 1000 1250 1500 2000
do
for mp in 1 2 5 10
do
for tp in 0p1 1 2 5 25 45 60 100 150 225 300 500
do
  #if [ $mx != 800 ] || [ $mp != 5 ] || [ $tp != 60 ]; then
  echo "yes | ./scan_{??}.py -x $mx -p $mp -t $tp  -n 80Xb >>modelrunoutput.log"
  yes | ./scan_gN1.py -x $mx -p $mp -t $tp  -n 80Xb >>modelrunoutput.log
  if [ $? -eq 1 ];then
	echo "nosuchmodel"
  else
    ./massjobs.sh
  fi
#  else 
#  echo "ignore -x $mx -p $mp -t $tp"
  #fi
done 
done
done
