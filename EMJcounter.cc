
#include"paramSet.h"


const Long64_t printfreq = 10000;
int EMJcounter(TChain *ch,  std::vector<Parmset> &psv, const char* outputfilename, TH1F* hpTrig) { 

  std::cout<<"Entering EMJcounter"<<std::endl; 
  bool otfile=true;
  bool hasPUcut=true;
  int npass=0;
  int ncut = psv.size();
std::cout<<"number of cuts"<<ncut<< std::endl; 
  //TFile *f = new TFile(inputfilename);
  //std::cout<<"inputfile: "<<inputfilename<<std::endl;
  //TTree *ch = (TTree*)f->Get("emJetAnalyzer/emJetTree");

//define ntuple variables

  Int_t nVtx, event, lumi, run, nTrueInt, nTracks;//, pv_index;
  Bool_t hltbit = 1;
  Float_t met_pt, met_phi;

  //pv                                                                              
  //Float_t pv_x,pv_y,pv_z;
  vector<float> *pv_x = 0;
  vector<float> *pv_y = 0;
  vector<float> *pv_z = 0;

  vector<int> *pv_index=new vector<int>;
  vector<int> *jet_index=new vector<int>;
  vector<int> *jet_source=new vector<int>;
  vector<float> *jet_pt = new vector<float>;
  vector<float> *jet_eta = new vector<float>;
  vector<float> *jet_phi = new vector<float>;
  vector<float> *jet_cef = new vector<float>;
  vector<float> *jet_nef = new vector<float>;
  vector<float> *jet_chf = new vector<float>;
  vector<float> *jet_nhf = new vector<float>;
  vector<float> *jet_theta2D = new vector<float>;
  //  vector<float> *jet_phf = new vector<float>;
  vector<vector<float> > *track_pt = 0;
  vector<vector<float> > *track_eta = 0;
  vector<vector<float> > *track_pvWeight =0;
  vector<vector<int> > *track_source = 0;
  vector<vector<int> > *track_quality = 0;
  vector<vector<int> > *track_index = 0;
  vector<vector<int> > *track_jet_index = 0;
  vector<vector<int> > *track_vertex_index = 0;
  vector<vector<int> > *track_algo = 0;
  vector<vector<float> > *track_vertex_weight =0;
  vector<vector<float> > *track_ipZ =0;
  vector<vector<float> > *track_ipXY = 0;
  vector<vector<float> > *track_ipXYSig = 0;
  vector<vector<float> > *track_ip3DSig = 0;
  vector<vector<float> > *track_ref_x =0;
  vector<vector<float> > *track_ref_y =0;
  vector<vector<float> > *track_ref_z =0;
  vector<vector<int> > *track_nMissInnerHits = 0;
  vector<vector<int> > *track_nMissInnerPxlLayers = 0;
  vector<vector<int> > *track_nPxlLayers = 0;
  vector<vector<int> > *track_nHits = 0;


std::cout<< "Setting Branch address"<<std::endl;
//for ntuple
{
  ch->SetBranchAddress("nVtx",&nVtx);
  ch->SetBranchAddress("pv_index",&pv_index);
  ch->SetBranchAddress("nTrueInt",&nTrueInt);
  ch->SetBranchAddress("nTracks",&nTracks);
  ch->SetBranchAddress("pv_x",&pv_x);
  ch->SetBranchAddress("pv_y",&pv_y);
  ch->SetBranchAddress("pv_z",&pv_z);
  ch->SetBranchAddress("event",&event);
  ch->SetBranchAddress("lumi",&lumi);
  ch->SetBranchAddress("run",&run);
  ch->SetBranchAddress("met_pt",&met_pt);
  ch->SetBranchAddress("met_phi",&met_phi);
  ch->SetBranchAddress("jet_index",&jet_index);
  ch->SetBranchAddress("jet_source",&jet_source);
  ch->SetBranchAddress("jet_pt",&jet_pt);
  ch->SetBranchAddress("jet_eta",&jet_eta);
  ch->SetBranchAddress("jet_phi",&jet_phi);
  ch->SetBranchAddress("jet_theta2D",&jet_theta2D);
  ch->SetBranchAddress("jet_cef",&jet_cef);
  ch->SetBranchAddress("jet_nef",&jet_nef);
  ch->SetBranchAddress("jet_chf",&jet_chf);
  ch->SetBranchAddress("jet_nhf",&jet_nhf);
  ch->SetBranchAddress("track_pt",&track_pt);
  ch->SetBranchAddress("track_eta",&track_eta);
  ch->SetBranchAddress("track_pvWeight",&track_pvWeight);
  ch->SetBranchAddress("track_source",&track_source);
  ch->SetBranchAddress("track_quality",&track_quality);
  ch->SetBranchAddress("track_index",&track_index);
  ch->SetBranchAddress("track_jet_index",&track_jet_index);
  ch->SetBranchAddress("track_algo",&track_algo);
  ch->SetBranchAddress("track_vertex_index",&track_vertex_index);
  ch->SetBranchAddress("track_vertex_weight",&track_vertex_weight);
  ch->SetBranchAddress("track_ipXY",&track_ipXY);
  ch->SetBranchAddress("track_ipXYSig",&track_ipXYSig);
  ch->SetBranchAddress("track_ip3DSig",&track_ip3DSig);
  ch->SetBranchAddress("track_ref_x",&track_ref_x);
  ch->SetBranchAddress("track_ref_y",&track_ref_y);
  ch->SetBranchAddress("track_ref_z",&track_ref_z);
  ch->SetBranchAddress("track_nMissInnerHits",&track_nMissInnerHits);
  ch->SetBranchAddress("track_nMissInnerPxlLayers",&track_nMissInnerPxlLayers);
  ch->SetBranchAddress("track_nPxlLayers",&track_nPxlLayers);
  ch->SetBranchAddress("track_nHits",&track_nHits);
  ch->SetBranchAddress("track_ipZ",&track_ipZ);
  ch->SetBranchAddress("HLT_PFHT900",&hltbit);

}

  std::cout<< "instantiate histograms"<<std::endl; 

  // create a histograms
  std::vector<TH1F*> hcounta, hcount, hcountjet;

for (int icut=0;icut<ncut;icut++){
  hcounta.push_back(new TH1F(("hcounta"+std::to_string(icut)).c_str(),"counts",20,0.,20.));
  hcount.push_back(new TH1F(("hcount"+std::to_string(icut)).c_str(),"counts",16,0,16));
  hcountjet.push_back(new TH1F(("hcountjet"+std::to_string(icut)).c_str(),"cut flow of leading 4 jets",15,0,15));
  hcount[icut]->SetStats(0);
  hcountjet[icut]->SetStats(0);
  hcount[icut]->SetCanExtend(TH1::kAllAxes);
  hcountjet[icut]->SetCanExtend(TH1::kAllAxes);
  { Parmset ps = psv.at(icut);
    std::cout<< "Cut set #"<<icut<<std::endl;  
    printParam(ps);
    std::cout<< " "<<std::endl;  }
  }

  std::cout<< "Get Entries"<<std::endl; 
  //read all entries and fill the histograms
  Long64_t nentries = ch->GetEntries();
  std::cout<< "TChain entries : "<<nentries<< std::endl;
  if (hasPUcut) std::cout<< "pile up cut applied"<<std::endl; 

  int entrysize=10;
// Kak: loop over events
  for (Long64_t i=0; i<nentries&&entrysize>0; i++) {
  //for (Long64_t i=0; i<printfreq*5; i++) 
 
    if (i%printfreq==0||(nentries<10000&&i%(nentries/100)==0)) std::cout<< "At entry #"<<i<<" ("<<i*100/nentries<<"%)"<<std::endl; 
    //if(!hasPre) hpTrig->Fill(1.5); 
    if (otfile){ for (int icut=0;icut<ncut;icut++){
      hcount[icut]->Fill("All",1);  // count number of events
      hcounta[icut]->Fill(0.5);
    } }
    
    entrysize=ch->GetEntry(i);
    if (entrysize<=0){
	std::cerr<<"entry fail "<<entrysize<<" entry#"<<i<<std::endl; 
	break;
    } 

    // jets
    Int_t jetsize = jet_index->size();
    vector<int>    jet_ntrkpt1(jetsize);
    vector<float>  jet_meanip(jetsize);
    vector<double> jet_fpt(jetsize);
    vector<double> jet_ptmax(jetsize);
    vector<double> jet_fnpile(jetsize);
    vector<float> AM(jetsize);
    vector<float> A2D(jetsize);
    vector<float> A2DSIG(jetsize);
    vector<float> F2DSIG(jetsize);
    vector<float> A3D(jetsize);
    vector<float> r0(jetsize);
    vector<float> r1(jetsize);
    vector<float> rmed(jetsize);
    vector<int>   jntrack(jetsize);
    vector<float> jet_e(jetsize);
    vector<float> jet_theta(jetsize);
    vector<float> jet_px(jetsize);
    vector<float> jet_py(jetsize);
    vector<float> jet_pz(jetsize);
    vector<float> jet_met(jetsize);
// NOTE: LOOP OVER CUTSETS
for (int icut=0;icut<ncut;icut++){

    Parmset ps = psv.at(icut);
    // PVtrack fraction
    int   nTrack_total = 0; int   nPVTrack=0;
    // Kak: looping over each jet
    for(Int_t j=0; j<jetsize; j++) {
      float jpt = jet_pt->at(j);
      float jeta= jet_eta->at(j);
      float jphi= jet_phi->at(j);
      jet_theta[j]=2.*atan(exp(-jeta));
      jet_e[j]=jpt/sin(jet_theta[j]);
      jet_px[j]=jpt*cos(jphi);
      jet_py[j]=jpt*sin(jphi);
      jet_pz[j] = sinh(jeta)*jpt;
      jet_met[j]=met_pt*cos(jphi-met_phi);

        
      //      calculate track variables
      jet_ntrkpt1[j]=0;
      jet_meanip[j]=0.;
      AM[j]=-1.;
      A2D[j]=-1.;
      A2DSIG[j]=-1.;
      F2DSIG[j]=-1;
      jet_fpt[j]=0.;
      jet_ptmax[j]=0.;
      jet_fnpile[j]=0.;
      if(r0.size()>0) r0[j]=0.;
      if(r1.size()>0) r1[j]=0.;
      if(rmed.size()>0) rmed[j]=0.;
      vector<float> track_pts       = track_pt->at(j);
      vector<float> track_pvWeights = track_pvWeight->at(j);
      vector<int>   track_sources   = track_source->at(j);
      vector<int>   track_qualitys  = track_quality->at(j);
      vector<float> track_vertex_weights  = track_vertex_weight->at(j);
      vector<float> track_ipXYs     = track_ipXY->at(j);
      vector<float> track_ipXYSigs  = track_ipXYSig->at(j);
      vector<float> track_ip3DSigs  = track_ip3DSig->at(j);
      vector<float> sort_ip;
      vector<float> track_ref_xs    = track_ref_x->at(j);
      vector<float> track_ref_ys    = track_ref_y->at(j);
      vector<float> track_ref_zs    = track_ref_z->at(j);
      //double sumpt=0.;
      double sumpile=0.;
      double sumptall=0.;
      double sumptallnz=0.;
      //double sumpt2d=0.;
      //double sumptsig=0.;
      double sumpta3d=0.;
      //int    tracks_ipsig=0;
      jntrack[j]=0;
      int   tracks_srczero = 0; // for counting number of tracks src 0
      //int   iptmaxtrk=0;          // Kak: index for max pt trk
      float ptmaxtrk=0.;        // Kak: pt of max pt trk

      // Kak: track kinematic cuts
      for (unsigned itrack=0; itrack<track_pts.size(); itrack++) {
        if((track_sources[itrack]==0)&&((track_qualitys[itrack]&4)>0)) { // source and quality
          sumptallnz+=track_pts[itrack];
	  nTrack_total++;
	  if ( fabs(pv_z->at(0)-track_ref_zs[itrack])<0.01 ) nPVTrack++;
          if (!hasPUcut||fabs(pv_z->at(0)-track_ref_zs[itrack])<ps.pvz_cut)  // pileup cut 
 	  {
            tracks_srczero ++;
            // Kak: max track pt recorded
            if(track_pts[itrack]>ptmaxtrk) ptmaxtrk=track_pts[itrack];
            sort_ip.push_back(fabs(track_ipXYs[itrack]));

            // Kak: sumptall: sum of pt of all tracks passing qual cuts
            sumptall+=track_pts[itrack];
            // float ahate = fabs(track_ip3DSigs[itrack]);
	    float ahate = fabs(pv_z->at(0)-track_ref_zs[itrack]);
            ahate=ahate/ps.a3dzcut;  // 0.01 is guess on width?
            float ahate2=track_ipXYSigs[itrack];
            ahate =ahate*ahate+ahate2*ahate2;
            ahate=sqrt(ahate);
            if(ahate<ps.ahatecut) sumpta3d+=track_pts[itrack];
            // Kak: non-pile-up sum pt
   	    if (fabs(pv_z->at(0)-track_ref_zs[itrack])<ps.pvz_cut) sumpile+=track_pts[itrack];//PU track for nonPU traction 
            if(track_pts[itrack]>1) jet_ntrkpt1[j]+=1;
            jntrack[j]++; 
            if (jntrack[j]!=tracks_srczero) std::cout<<"!srczero warning: "<<tracks_srczero<<" "<<jntrack[j]<<std::endl;
          } // pileup cut
        } // source and quality
      }//track kinematics

      if(sumptall>0)
      {
        A3D[j]=sumpta3d/sumptall;
        //AM[j]=sumpt/sumptall;
        //A2D[j]=sumpt2d/sumptall;
        //A2DSIG[j]=sumptsig/sumptall;
      }

      // Kak: fraction nonPU
      if(sumptallnz>0) jet_fnpile[j]=sumpile/sumptallnz;
      // Kak: fraction of max pt to total track pt
      jet_fpt[j]=ptmaxtrk/jet_pt->at(j);
      jet_ptmax[j]=ptmaxtrk;

      // Kak: sort ip & median ip calculation
      std::sort(sort_ip.begin(), sort_ip.end());
      std::reverse(sort_ip.begin(),sort_ip.end());
      if(sort_ip.size()>0) r0[j]   = sort_ip[0];
      if(sort_ip.size()>1) r1[j]   = sort_ip[1];
      if(sort_ip.size()>0) rmed[j] = median(sort_ip);  

    //      std::cout<<"mean max are "<<jet_meanip[j]<<" "<<r0[j]<<std::endl;
    }  // end of loop over jets

    double pvtrackfraction = (double) nPVTrack/nTrack_total;

//now see which jets are emerging

    //    std::cout<<" in event "<<event<<" number of jets is "<<jetsize<<std::endl;
    vector<bool> emerging(jetsize);
    vector<bool> almostemerging(jetsize);
    vector<bool> basicjet(jetsize);
    for( int j=0;j<4;j++) {
      emerging[j]=false;
      almostemerging[j]=false;
      basicjet[j]=false;
    }
    int nbasicjet=0;
    int nemerging_a3d=0;
    int nemerging_medip=0;
    int nemerging_medipanda3d=0;
    int nemerging_jmet=0;
    int nemerging=0;
    int nalmostemerging=0;
    bool hasHighmedip=false;
    for(int ij=0;ij<jetsize;ij++) { // loop over leading jets
      if(otfile && ij<4) hcountjet[icut]->Fill("all",1);
      if(fabs(jet_eta->at(ij))<ps.jetacut) { // jet eta cut
        if(otfile && ij<4) hcountjet[icut]->Fill("eta",1);
        if(jet_nef->at(ij)<ps.NemfracCut) {  // neutral fraction
          if(otfile && ij<4) hcountjet[icut]->Fill("nef",1);
          if(jet_cef->at(ij)<ps.CemfracCut) {  //charged fraction
	    if(otfile && ij<4) hcountjet[icut]->Fill("cef",1);
            if(jet_ntrkpt1[ij]>ps.ntrk1cut) {  // tracks pt>1
	       if(otfile && ij<4) hcountjet[icut]->Fill("trkpt1",1);

      // Kak: if loop level 4
      if(jet_fpt[ij]<0.6) { // jet fpt
	    if(otfile && ij<4) hcountjet[icut]->Fill("fpt",1);
        //if (jet_fnpile[ij]>0.4)
  	      { // jet fnpile
	      if(otfile && ij<4) hcountjet[icut]->Fill("fnPU/basic",1);

          basicjet[ij]=true;
          if(ij<4) nbasicjet+=1;
          if(rmed[ij]>ps.medIPcut) { // med IP cut  
              if(ij<4) nemerging_medip+=1.;
	      if(otfile && ij<4) hcountjet[icut]->Fill("medIP",1);
           } // med IP cut

          if (A3D[ij]<ps.a3dcut){
              almostemerging[ij]=true;
              if(ij<4) nalmostemerging=nalmostemerging+1;
	      if(otfile && ij<4) hcountjet[icut]->Fill("a3d/alEmrg",1);
              if(ij<4) nemerging_a3d+=1.;
   	      if(rmed[ij]>ps.medIPcut) { // med IP cut  
                  if(ij<4) nemerging_medipanda3d+=1.;
	          if(otfile && ij<4) hcountjet[icut]->Fill("a3dmIP",1);
	      }
   	      if(rmed[ij]>ps.medIPcut||jet_met[ij]>ps.jmetcut) { // med IP cut  
                  if(ij<4) nemerging_jmet+=1.;
	          if(otfile && ij<4) hcountjet[icut]->Fill("jmet/Emrg",1);
                  if(jet_theta2D->at(ij)>ps.theta2dcut) {
                      emerging[ij]=true;
                      if(ij<4) nemerging+=1.;
                  } //theta2D
	      }
          } // alpha max/ a3d

        } // jet fnpile
      } // jet fpt

            } // ntrkpt1
          } // charged fraction
        } // neutral fraction
      } // jet eta cut

     /* if (i==0) { //cout first event
        std::cout<<"Jet #"<<ij<<": "<<"am"<<AM[ij]<<" a2dsig "<<A2DSIG[ij]<<" "<<rmed[ij]<<" "<<jet_theta2D->at(ij)<<std::endl;
        bool ctAm= AM[ij]<ps.alphaMaxcut;
        bool ctA2sig = (A2DSIG[ij]<ps.a2dsigcut);
        bool ctMip = rmed[ij]>ps.medIPcut;
        bool ctT2D = jet_theta2D->at(ij)>ps.theta2dcut;
        std::cout<<"jetpass "<<basicjet[ij]<<" "<<ctAm<<" "<<ctA2sig<<" "<<ctMip<<" "<<ctT2D<<" bsJ "<<basicjet[ij]<<" em"<<emerging[ij] << " a2dsig"<<A2DSIG[ij]<<std::endl;
      }*/
    } 
    // loop over leading jets
    //std::cout<<"event"<<event<<" "<<fabs(jet_eta->at(0))<<" "<<jet_nef->at(0)<<
    //" "<<jet_ntrkpt1[0] <<" "<<jet_cef->at(0)<<" "<<jet_fpt[0]<<" "<<jet_fnpile[0]<<" "<< std::endl;
    
    
    
    // find best mass pairing
    /*float amass = -1.;
    if(jetsize>3) {
      int ie1 =-1;
      int ie2=-1;
      int id1=-1;
      int id2=-1;

      for(int i5=0;i5<4;i5++) {
        if(emerging[i5]) {
          if(ie1==-1) {ie1=i5;}
          else        {ie2=i5;}
        } else {
          if(id1==-1) {id1=i5;}
          else        {id2=i5;}
        }
      }
      if(ie1>-1&&ie2>-1&&id1>-1&&id2>-1) {
      float mass1a = masspair(jet_e,jet_px,jet_py,jet_pz,ie1,id1);
      float mass1b = masspair(jet_e,jet_px,jet_py,jet_pz,ie2,id2);
      float mass2a = masspair(jet_e,jet_px,jet_py,jet_pz,ie1,id2);
      float mass2b = masspair(jet_e,jet_px,jet_py,jet_pz,ie2,id1);
      if(fabs(mass1a-mass1b)<fabs(mass2a-mass2b)) {
        amass=(mass1a+mass1b)/2.;
      } else {
        amass=(mass2a+mass2b)/2.;
      }
      }
    }*/



  /*************************************************************
  now start the event selections
  *************************************************************/

    // vertex selection

    // Kak: AOD original sorting && pt2sum sorting must agree
    bool PVPT0=true;
    if((*pv_index).at(0)>0) PVPT0=false;

    bool PVZ=true;
    if(fabs(pv_z->at(0))>15) PVZ=false;

    bool Cfpvtrk = (pvtrackfraction>0.1);

    // require at least 4 jets
    bool C4jet=true;
    if(nbasicjet<4) C4jet=false;
    //if (C4jet) std::cout<<"4jet ";
    // HT
    double HT = 0;
    int iijjkk=std::min(jetsize,4);
    for(int i=0;i<iijjkk;i++) {
      HT+=jet_pt->at(i);
    }

    bool CHT=true;
    if(!hltbit||HT<ps.HTcut) CHT=false;
    // jet pt
    bool Cpt1=false;
    bool Cpt2=false;
    bool Cpt3=false;
    bool Cpt4=false;
    if(jetsize>0)
    if((jet_pt->at(0)>ps.pt1cut)&&(fabs(jet_eta->at(0))<ps.jetacut)) Cpt1=true;
    if(jetsize>1)
    if((jet_pt->at(1)>ps.pt2cut)&&(fabs(jet_eta->at(1))<ps.jetacut)) Cpt2=true;
    if(jetsize>2)
    if((jet_pt->at(2)>ps.pt3cut)&&(fabs(jet_eta->at(2))<ps.jetacut)) Cpt3=true;
    if(jetsize>3)
    if((jet_pt->at(3)>ps.pt4cut)&&(fabs(jet_eta->at(3))<ps.jetacut)) Cpt4=true;
    // number emerging jets
    bool Cnem_basic = (nbasicjet>=ps.NemergingCut);
    bool Cnem_a3d = (nemerging_a3d>=ps.NemergingCut);
    bool Cnem_medip = (nemerging_medip>=ps.NemergingCut);
    bool Cnem_medipanda3d = (nemerging_medipanda3d>=ps.NemergingCut);
    bool Cnem_jmet = (nemerging_jmet>=ps.NemergingCut);
    bool Cnem = true;
    if(nemerging<ps.NemergingCut) Cnem=false;

    bool Canem =true;
    //if(nalmostemerging>=4) Canem=false;

    //bool Cmass = false;
    //if(fabs(amass-ps.mass)<ps.masscut) Cmass=true;

    bool Cmet = false;
    if(met_pt>ps.metcut) Cmet = true;
    
    if (hasHighmedip) std::cout<<CHT<<Cpt1<<Cpt2<<Cpt3<<Cpt4<<Cnem<<Canem<<Cmet<<std::endl;
 // apply cuts sequentially
    if(PVZ) {
    if(PVPT0) {
    //if(otfile) hcount[icut]->Fill("filter",1);
    //if(otfile) hcounta[icut]->Fill(1.5);

    if(Cfpvtrk){
      if(otfile) hcount[icut]->Fill("fPVtrk",1);
      if(otfile) hcounta[icut]->Fill(1.5);
    if(C4jet) {
      if(otfile) hcount[icut]->Fill("4 jets",1);
      if(otfile) hcounta[icut]->Fill(2.5);
      
      // calculate HT and require it greater than some cut value
      if(CHT) {
  if(otfile) hcount[icut]->Fill("HT",1);
  if(otfile) hcounta[icut]->Fill(3.5);
  
  // do pT cuts on jets  
  if(Cpt1) {
    if(otfile) hcount[icut]->Fill("jet pt1",1);
    if(otfile) hcounta[icut]->Fill(4.5);
    
    
    if(Cpt2) {
      if(otfile) hcount[icut]->Fill("jet pt2",1);
      if(otfile) hcounta[icut]->Fill(5.5);
      
      
      if(Cpt3) {
        if(otfile) hcount[icut]->Fill("jet pt3",1);
        if(otfile) hcounta[icut]->Fill(6.5);
        
        
        if(Cpt4) {
    if(otfile) hcount[icut]->Fill("jet pt4",1);
    if(otfile) hcounta[icut]->Fill(7.5);
    if(Cmet) {
        if(otfile) hcount[icut]->Fill("met cut",1);
        if(otfile) hcounta[icut]->Fill(8.5);
    
        //if (Cnem_basic && otfile) hcount[icut]->Fill("emerg/basic",1);
        //if (Cnem_basic && otfile) hcounta[icut]->Fill(9.5);
        if (Cnem_a3d && otfile) hcount[icut]->Fill("emerg/a3d",1);
        if (Cnem_a3d && otfile) hcounta[icut]->Fill(9.5);
     //   if (Cnem_medip && otfile) hcount[icut]->Fill("emerg/medip",1);
       // if (Cnem_medip && otfile) hcounta[icut]->Fill(11.5);
        if (Cnem_medipanda3d && otfile) hcount[icut]->Fill("emerg/mip",1);
        if (Cnem_medipanda3d && otfile) hcounta[icut]->Fill(10.5);
        //if (Cnem_jmet && otfile) hcount[icut]->Fill("emerg/jmet",1);
        //if (Cnem_jmet && otfile) hcounta[icut]->Fill(13.5);
    
    
    // require at least N emerging jets
    if(Cnem) {
      //std::cout<<"PASS without almost"<<std::endl;
      //std::cout<<"nEmerge NalmostEm is "<<nemerging<<" "<<nalmostemerging<<" "<<<Canem<<std::endl;
      
      
      //if(otfile) hcount[icut]->Fill("emerging",1);
      if(otfile) hcounta[icut]->Fill(11.5);
      if(Canem) {
        //std::cout<<"PASS with almost"<<std::endl;
        
       // if(otfile) hcount[icut]->Fill("almostemerging",1);
        if(otfile) hcounta[icut]->Fill(12.5);

       // if(Cmass) {
       // if(otfile) hcount[icut]->Fill("mass cut",1);
      //  if(otfile) hcounta[icut]->Fill(16.5);


        
          npass+=1;
                
          //std::cout<<"npass  event is "<<npass<<" "<<event<<std::endl;
          //std::cout<<"nemerging nalmostemerging "<<nemerging<<" "<<nalmostemerging<<std::endl;
       // } //mass
      } //anem
    } //nem
        } //met
        } //Cpt4
      }
    }
  } //Cpt1
      }
    }
    } }    }
/*  if (i%printfreq==0) {
    std::cout<<"cutpasses: "<<PVZ<<" "<<PVPT0<<" "<<C4jet<<" "<<CHT<<" "<<Cpt1<<" "<<Cpt2
    <<" "<<Cpt3<<" "<<Cpt4<<" "<<Cnem<<" "<<Canem<<" "<<Cmass<<" "<<Cmet<<
    " mass "<< amass<<" "<<nalmostemerging<<" "<<nemerging<<std::endl;
  }
 */ }  //NOTE: finish looping over cutsets
  }  // end of loop over events

  if(otfile) {
    TFile myfile(outputfilename,"RECREATE");
    hpTrig->Write();
    for (int icut=0;icut<ncut;icut++){
       hcount[icut]->LabelsDeflate();
       hcountjet[icut]->LabelsDeflate();
       hcount[icut]->LabelsOption("v");
       hcountjet[icut]->LabelsOption("v");
	std::cerr<<"Labelsoption\n";
       hcounta[icut]->Write();
       hcount[icut]->Write();
       hcountjet[icut]->Write();
    }
    myfile.Close();
  }

  ch->ResetBranchAddresses();
  
  { //delete
      delete jet_index;
      delete jet_source;
      delete jet_pt;
      delete jet_eta;
      delete jet_phi;
      delete jet_cef;
      delete jet_nef;
      delete jet_chf;
      delete track_pt;
      delete track_eta;
      delete track_source;
      delete track_index;
      delete track_jet_index;
      delete track_vertex_index;
      delete track_algo;
      delete track_vertex_weight;
      delete track_ipZ;
      delete track_ipXY;
      delete track_ipXYSig;
      delete track_ip3DSig;
      delete track_ref_x;  
      delete track_ref_y;  
      delete track_ref_z; 
  } //delete

 
  


  //f->Close();
  // delete f;  


  return npass;
}
