#!/bin/bash

#export VO_CMS_SW_DIR=/sharesoft/cmssw
#. $VO_CMS_SW_DIR/cmsset_default.sh
#cd /home/kakw/CMSSW_8_1_0/src/cutflow/cutopt 
#eval `scramv1 runtime -sh`
suf=$1 #"_ptxd0"

#echo "loop.sh $1 $2"

echo "mergeTFileServiceHistograms -i kinscan_QCD_${suf}/histosQCD_HT500to700*.root -o kinscan_QCD_${suf}/sumhistosHT500to700.root"
#mergeTFileServiceHistograms -i kinscan_QCD2_${suf}/histosQCD_HT500to700*.root -o kinscan_QCD2_${suf}/sumhistosHT500to700.root
#mergeTFileServiceHistograms -i kinscan_QCD2_${suf}/histosQCD_HT700to1000*.root -o kinscan_QCD2_${suf}/sumhistosHT700to1000.root
#mergeTFileServiceHistograms -i kinscan_QCD2_${suf}/histosQCD_HT1000to1500*.root -o kinscan_QCD2_${suf}/sumhistosHT1000to1500.root
#mergeTFileServiceHistograms -i kinscan_QCD2_${suf}/histosQCD_HT1500to2000*.root -o kinscan_QCD2_${suf}/sumhistosHT1500to2000.root
#mergeTFileServiceHistograms -i kinscan_QCD2_${suf}/histosQCD_HT2000toInf*.root -o kinscan_QCD2_${suf}/sumhistosHT2000toInf.root
# mergeTFileServiceHistograms -i kinscan_modB_${suf}/histosmodelB-0-*.root -o kinscan_modB_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modA_${suf}/histosmodelA-0-*.root -o kinscan_modA_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD1_${suf}/histosmodelD1-0-*.root -o kinscan_modD1_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD2_${suf}/histosmodelD2-0-*.root -o kinscan_modD2_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD3_${suf}/histosmodelD3-0-*.root -o kinscan_modD3_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD4_${suf}/histosmodelD4-0-*.root -o kinscan_modD4_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD5_${suf}/histosmodelD5-0-*.root -o kinscan_modD5_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD1mm_${suf}/histosmodelD1mm-0-*.root -o kinscan_modD1mm_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modD150mm_${suf}/histosmodelD150mm-0-*.root -o kinscan_modD150mm_${suf}/sumhistos${y}.root

# mergeTFileServiceHistograms -i kinscan_modF1_${suf}/histosmodelF1-0-*.root -o kinscan_modF1_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF2_${suf}/histosmodelF2-0-*.root -o kinscan_modF2_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF3_${suf}/histosmodelF3-0-*.root -o kinscan_modF3_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF4_${suf}/histosmodelF4-0-*.root -o kinscan_modF4_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF5_${suf}/histosmodelF5-0-*.root -o kinscan_modF5_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF6_${suf}/histosmodelF6-0-*.root -o kinscan_modF6_${suf}/sumhistos${y}.root
# mergeTFileServiceHistograms -i kinscan_modF7_${suf}/histosmodelF7-0-*.root -o kinscan_modF7_${suf}/sumhistos${y}.root


echo "mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT500to700*.root -o kinscan_QCD4_${suf}/sumhistosHT500to700.root 2>loop.stderr"
mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT500to700*.root -o kinscan_QCD4_${suf}/sumhistosHT500to700.root 2>loop.stderr
mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT700to1000*.root -o kinscan_QCD4_${suf}/sumhistosHT700to1000.root 2>loop.stderr
mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT1000to1500*.root -o kinscan_QCD4_${suf}/sumhistosHT1000to1500.root 2>loop.stderr
mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT1500to2000*.root -o kinscan_QCD4_${suf}/sumhistosHT1500to2000.root 2>loop.stderr
mergeTFileServiceHistograms -i kinscan_QCD4_${suf}/histosQCD_HT2000toInf*.root -o kinscan_QCD4_${suf}/sumhistosHT2000toInf.root 2>loop.stderr
#EOF
