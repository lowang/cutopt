#!/bin/bash

set -e
# create config/parms files for alternative models

#workdir=$PWD
#echo $workdir
#rm -f models_80Xb_20171103.txt
#for fullpath in /store/user/yoshin/EmJetAnalysis/Analysis-20170823-v0/mass_X_d_1000_mass_pi_d_*_tau_pi_d*
#for fullpath in /data/users/yhshin/EmJetAnalysis/Analysis-20171002-v2/mass_X_d_*_mass_pi_d*
#for fullpath in  /store/user/yoshin/EmJetAnalysis/Analysis-20171103-v1/mass_X_d_*_mass_pi_d*
for fullpath in  /store/user/yoshin/EmJetAnalysis/Analysis-20171103-v1b/mass_X_d_*_mass_pi_d*
#for fullpath in /store/user/yoshin/EmJetAnalysis/Analysis-20170811-v1/mass_X_d_1000_mass_pi_d_*_tau_pi_d*
#cd /store/user/abelloni/EmJetAnalysis/Analysis-74xAODSIM_80xNTUPLES-v1
#for fullpath in /store/user/abelloni/EmJetAnalysis/Analysis-74xAODSIM_80xNTUPLES-v1/*/
  do folder=${fullpath%/} 
  folder=${folder##*/}
  configpath=model_80Xb_20171103/${folder}_80X_config.txt
  parmspath=modelparms_80Xb_20171103/parms_80X_${folder}.txt
  cp parms_template.txt ${parmspath}
  fnum=`ls -1 ${fullpath}/*/*/*/*/ntuple_*.root | wc -l`
  echo ${folder} $fnum
  echo ${folder}>>models_80Xb_20171103.txt
  sed -i 's/DIRNAME/'${folder}'_80Xb/g' ${parmspath} #binname
  sed -i 's/NUM/'${fnum}'/g' ${parmspath}
  echo 's/CONFIGFILE/configs/'${configpath}'/g' 
  sed -i 's/CONFIGFILE/configs\/model_80Xb_20171103\/'${folder}'_80X_config.txt/g' ${parmspath} 
  ls -1 ${fullpath}/*/*/*/*/ntuple_*.root > ${configpath}
done
